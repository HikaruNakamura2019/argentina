<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<title>ツクトモ</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<!-- BootstrapのCSS読み込み -->
<link href="css/bootstrap.min.css" rel="stylesheet">
<!-- jQuery読み込み -->
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- BootstrapのJS読み込み -->
<script src="js/bootstrap.min.js"></script>

</head>
<body>
	<jsp:include page="menu.jsp" flush="true" />
	<div style="text-align: center">
		<br> <br>
		<h1>LOGIN</h1>
		<form action="login" method="post">
			<div class="form-group">
				<label for="exampleInputEmail1">メールアドレス</label><br> <input
					type="email" size="40" maxlength="100" name="mail"
					id="exampleInputEmail1" aria-describedby="emailHelp"
					placeholder="メールアドレスを入力してください">
			</div>
			<div class="form-group">
				<label for="exampleInputPassword1">パスワード</label><br> <input
					type="password" size="40" maxlength="64" name="pass"
					id="exampleInputPassword1" placeholder="パスワードを入力してください">
			</div>
			<%
				String error = (String) request.getAttribute("error");
			%>
			<p style="color: red; font-size: larger;">
				<%
					if (error != null)
						out.println(error);
				%>

			<p>
				<button type="submit" class="btn btn-primary btn-lg">ログイン</button>
			</p>
		</form>

		<br> <a href="./userRegistration">新規登録</a><br> <a
			href="./index">トップページへ</a>
	</div>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
</body>


</html>