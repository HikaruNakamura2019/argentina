<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.ArrayList"%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
	<title>ツクトモ</title>
	<link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css">
</head>

<body class="bg-light">
	<jsp:include page="../../menu.jsp" flush="true" />
	<%
		List<String> eventHobbys = (List<String>)request.getAttribute("eventHobbys");
		ArrayList<String> hobbyList = (ArrayList<String>)request.getAttribute("hobbyList");
	%>
	<div class="container">
		<div class="row">
			<div class="col-lg"></div>
			<div class="col-lg-8 container mb-3">
				<div class="row">
					<div class="col  text-center">
						<h1>イベント編集フォーム</h1>
					</div>
				</div>
				<div class="row">
					<form method="post" action="eventEdit" name="form">
						<div class="form-group">
							<label for="title">イベント名 </label>
							<input type="text" id="tlte" size="100" name="title" value="${eventTitle}"
							class="form-control" placeholder="※未入力だと変更なしです。" required>
						</div>
						<div class="form-group row">
							<div class="col-lg">
								<label for="day"> 締め切り(年月日)</label>
								<input type="date" class="form-control" name="date" id="date" min="1970-01-01"
									max="2038-01-09" value="${eventDate}" required />
							</div>
							<div class="col-lg">
								<label for="day"> 締め切り(時)</label>
								<div class="input-group">
									<input type="text" class="form-control" name="hour" id="hour"
										pattern="^([01][0-9]|2[0-3])$" placeholder="HH" value="${eventHour}" required>
									<div class="input-group-prepend">
										<span class="input-group-text">時</span>
									</div>
								</div>
							</div>
							<div class="col-lg">
								<label for="day"> 締め切り(分)</label>

								<div class="input-group">
									<input type="text" class="form-control" name="min" id="min"
										pattern="^([0-5][0-9])$" placeholder="mm" value="${eventMin}" required>
									<div class="input-group-prepend">
										<span class="input-group-text">分</span>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-lg">
								<label for="day"> 制限人数(人)</label>
								<div class="input-group">
									<input type="text" class="form-control" name="max_entry"
										id="max_entry" placeholder="例：31" value="${maxEntry}" required>
									<div class="input-group-prepend">
										<span class="input-group-text">人</span>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-lg">
								<label for="hobby_id">趣味タグ</label>
							</div>
						</div>
						<div class="form-group row">
							<%for(String hobby : hobbyList){ %>
								<div class="col-lg-3">
									<%if(eventHobbys.contains(hobby)){ %>
										<input type="checkbox" name="hobby" checked="checked" value=<%=hobby %> onchange="onChangeHobby();">
									<%}else{ %>
										<input type="checkbox" name="hobby" value=<%=hobby %> onchange="onChangeHobby();">
									<%} %>
									<label><%=hobby %></label>
								</div>
							<%} %>
						</div>
						<div id="errorHobby"></div>
						<div class="form-group">
							<label for="content">本文</label>
							<textarea id="content" name="content" class="form-control"rows="6" maxlength="300">${eventContent}</textarea>
						</div>
						<hr class="mb-4">
						<input type="hidden" name="eventId" value="${eventId}">
						<button class="btn btn-primary btn-lg btn-block" type="submit" id="submit">イベント内容 変更確定</button>
					</form>
				</div>
			</div>
			<div class="col-lg"></div>
		</div>
	</div>

	<script>
		function onChangeHobby() {
			let flag = false;

			const hobbys = document.getElementsByName("hobby");
			for (let i = 0; i < hobbys.length; i++) {
				if (hobbys[i].checked) {
					flag = true;
				}
			}

			if (!flag) {
				document.getElementById("errorHobby").innerHTML = `<div class="row">
					<div class="col-lg">
						<p style="color: red; font-size: larger;">趣味は必須項目です</p>
					</div>
				</div>`;

				document.getElementById("submit").setAttribute('disabled', 'true');
			} else {
				document.getElementById("errorHobby").innerHTML = "";
				document.getElementById("submit").removeAttribute('disabled');
			}
		}

	</script>

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
	</body>
</html>