<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.List,  java.net.URLEncoder"%>
<%@ page import="dto.UserDto"%>
<%@ page import="dao.EventDao"%>
<%@ page import="java.sql.Timestamp"%>
<html>
<head>
<meta charset="UTF-8">
<title>ツクトモ</title>

<!-- BootstrapのCSS読み込み -->
<link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
<!-- jQuery読み込み -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- BootstrapのJS読み込み -->
<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
<style>
table.event-info th, table.event-info td {
	border: solid 1px #ced4da;
	padding:5px;
}
table.event-info th{
	width:100px;
	padding-left:10px;
}
table.modal-table th, table.modal-info td{
	border:none;
}
div.event-text{
	min-height:190px
}
ul.modal-ul{
	list-style: none;
    line-height: 2em;
    font-size: 1.2em;
}
</style>
</head>
<body>
	<jsp:include page="../../menu.jsp" flush="true" />
	<%
		int eventId = (Integer)request.getAttribute("eventId");
		Timestamp currentTime = new Timestamp(System.currentTimeMillis());
		Timestamp eventDate = new EventDao().selectById(eventId).getEventDate();
		UserDto user = (UserDto) session.getAttribute("user");

	%>
	<div class="container">
		<div class="row">
			<div class="col-lg"></div>
			<div class="col-lg-8 container">
				<div class="row">
					<div class="col-lg">
						<h1 class="text-center mb-3">イベント詳細</h1>
					</div>
				</div>
				<div class="row">
					<div class="col-lg">
						<h2 class="mb-3"><%=request.getAttribute("title")%><br>
						<small class="text-muted">主催者：<%=request.getAttribute("ownerName")%></small>
						</h2>
					</div>
				</div>
				<div class="row">
					<div class="col-lg">
						<table class="table event-info">
							<tbody>
								<tr>
									<th class="bg-light">開催日</th>
									<td class="text-center"><%=request.getAttribute("eventDate")%></td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="col-lg">
						<table class="table event-info">
							<tbody>
								<tr>
									<th class="bg-light">参加状況</th>
									<td class="text-center">
										<span class="text-primary" data-toggle="modal" data-target="#entryList" style="cursor : pointer;">
											<%=request.getAttribute("entryUser")%>/<%=request.getAttribute("maxEntry")%>人
										</span>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<div class="row mx-3">
					<!-- hobby -->
					<%
						List<String> hobbys = (List<String>) request.getAttribute("hobbys");
						if (!hobbys.isEmpty()) {
							for (int i = 0; i < hobbys.size(); i++) {
								String hobby = hobbys.get(i);
					%>
					<button type="button" class="btn btn-info btn-sm"><%=hobby%></button>
					&emsp;
					<%
							}
						}
					%>
				</div>
				<div class="row">
					<div class="col-lg card p-4 m-3 event-text">
						<%=request.getAttribute("text")%>
					</div>
				</div>
				<form action="/Tsukutomo/entryUsersUpdate" method="post">
					<input type="hidden" name="eventId" value="<%=eventId %>">

					<% if(!(user != null && user.getUserId() == (Integer) request.getAttribute("ownerId"))) {
						if (currentTime.before(eventDate)) {
							if ((Boolean)request.getAttribute("isJoin")) {%>
								<button type="submit" class="btn  btn-danger btn-lg btn-block" name="action" value="decline">辞退</button>
							<% }else{%>
								<button type="submit" class="btn btn-primary btn-lg btn-block" name="action" value="join">参加</button>
							<% }
						}
					}else {%>
						<a class="btn btn-success btn-lg btn-block" href="/Tsukutomo/eventEdit/<%=request.getAttribute("eventId")%>">編集</a>
					<% }%>
				</form>


			</div>
<!-- Modal -->
	 <div class="modal fade" id="entryList" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	 	<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">参加者一覧</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body px-4">
					<ul class="modal-ul">
						<%
							List<String> names = (List<String>) request.getAttribute("entryUserNames");
							for (int i = 0; i < names.size(); i++) {
							String name = names.get(i);
						%>
								<li><a href="/Tsukutomo/direct/<%=name%>"><%=name%></a></li>
						<%
							}
						%>
					</ul>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				</div>
			</div>
		</div>
	</div>
<!-- Modal -->

			<div class="col-lg"></div>
		</div>
	</div>

	<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
	<script src="../js/bootstrap.min.js"></script>
</body>
</html>