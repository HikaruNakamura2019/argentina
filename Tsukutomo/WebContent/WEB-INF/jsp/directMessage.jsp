<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>ツクトモ</title>

  <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css">
  <link rel="stylesheet" href="${pageContext.request.contextPath}/css/main.css">

</head>
<body>
  <jsp:include page="../../menu.jsp" flush="true" />

  <div class="container">
    <div class="row">
      <div class="col"></div>
      <div class="user-list col-lg-1" id="user-list">
      </div>
      <div id="messages-card-container" class="mdl-grid col-lg-6">
        <div id="messages-card" class="card col-lg-12">
          <div class="card__supporting-text">
            <div id="messages">
              <span id="message-filler"></span>
            </div>
            <form id="message-form" action="#">
              <div class="textfield">
                <input type="text" id="message" placeholder="Message..." aria-describedby="basic-addon2">
              </div>
              <button type="submit" id="submit" disabled class="btn btn-primary btn-lg">Send</button>
            </form>
          </div>
        </div>
      </div>
      <div class="col"></div>
    </div>
  </div>

  <input type="hidden" id="from" value="${from}">
  <input type="hidden" id="to" value="${to}">

  <script src="https://www.gstatic.com/firebasejs/6.0.2/firebase-app.js"></script>
  <script src="https://www.gstatic.com/firebasejs/6.0.2/firebase-auth.js"></script>
  <script src="https://www.gstatic.com/firebasejs/6.0.2/firebase-database.js"></script>
  <script src="https://www.gstatic.com/firebasejs/6.0.2/firebase-firestore.js"></script>
  <script src="https://www.gstatic.com/firebasejs/6.0.2/firebase-messaging.js"></script>
  <script src="https://www.gstatic.com/firebasejs/6.0.2/firebase-functions.js"></script>

  <script>
    const config = {
      apiKey: "AIzaSyCHjV1R9AJyqytdXG4psrPUgyV0ybSF2xk",
      authDomain: "tsukutomo-argentina.firebaseapp.com",
      databaseURL: "https://tsukutomo-argentina.firebaseio.com",
      projectId: "tsukutomo-argentina",
    };
    firebase.initializeApp(config);

    function toggleUser(user) {
    	document.getElementById("to").value = user;
    	toggleToFrom();
    	var messages = document.getElementById("messages");
    	messages.innerHTML = '<span id="message-filler"></span>';
    	loadMessages();
    }
  </script>

<script src="${pageContext.request.contextPath}/js/main.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>]

</body>
</html>
