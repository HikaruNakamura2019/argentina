<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%	HttpSession s = request.getSession();%>

<div class="mb-5">
	<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
		<button class="navbar-toggler collapsed" type="button"
			data-toggle="collapse" data-target="#navbarsExample08"
			aria-controls="navbarsExample08" aria-expanded="false"
			aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="navbar-collapse justify-content-md-end collapse" id="navbarsExample08" style="">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item active"><a class="nav-link" href="/Tsukutomo/index">トップページ</a></li>
				<li class="nav-item active"><a class="nav-link" href="/Tsukutomo/eventRegistration">イベント作成</a></li>
				<li class="nav-item active"><a class="nav-link" href="/Tsukutomo/direct">DM</a></li>
				<li class="nav-item active"><a class="nav-link" href="/Tsukutomo/myPage">マイページ</a></li>
			</ul>
			<ul class="navbar-nav">
				<%if(s.getAttribute("user") == null ){%>
					<li class="nav-item active"><a class="nav-link" href="/Tsukutomo/login">ログイン</a></li>
				<%}else{ %>
					<li class="nav-item active"><a class="nav-link" href="/Tsukutomo/logout">ログアウト</a></li>
				<%} %>
			</ul>
		</div>
	</nav>
</div>