<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Arrays"%>
<%@ page import="java.util.HashMap"%>
<%@ page import="dto.EventDto"%>
<%@ page import="dao.EventDao"%>
<%@ page import="dto.UserDto"%>
<%@ page import="java.util.List"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ツクトモ</title>
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/common.css">
	<style>
	p {
		font-size:20px;
		line-heght:50px;
		text-indent:4em;
		text-align:center;
	}
	</style>
</head>
<body class="bg-light">
	<jsp:include page="menu.jsp" flush="true" />
	<%
	String name = (String) request.getAttribute ("username");
	String gender = (String) request.getAttribute ("gender");
	List<String> hobbyList=(List<String>) request.getAttribute ("hobbyList");
	ArrayList<EventDto> eventList =  (ArrayList<EventDto>)request.getAttribute("eventList");
	%>

	<div class="container">
		<div class="row">
			<div class="col-lg"></div>
			<div class="col-lg-8 container">
				<div class="row">
					<div class="col-lg">
						<h1 class="text-center mb-2">マイページ</h1>
					</div>
				</div>
				<div class="row bg-white p-2">
					<div class="col-lg-12">
						<div class="row mb-3">
							<h2>プロフィール</h2>
						</div>
						<div class="row">
							<p>ユーザー名：<%= name%></p>
						</div>
						<div class="row">
							<p>性別：<%= gender %></p>
						</div>
						<div class="row">
							<p>趣味</p>
						</div>
						<div class="row">
							<div class="col-lg-1"></div>
							<div class="col-lg">
								<h5>
									<td colspan="2" style="border-top: none;">
										<%for(String hobby : hobbyList){ %>
											<button type="button" class="btn btn-info" disabled><%= hobby%></button>
										<%} %>
									</td>
								</h5>
							</div>
						</div>
						<div class="row">
							<div class="col-lg">
								<p class="text-right">
									<a href="./userEdit">プロフィールを編集</a>
								</p>
							</div>
						</div>
					</div>
				</div>
				<hr class="mb-4">
				<div class="row">
					<h2>イベント一覧</h2>
				</div>
				<%for(EventDto event : eventList){ %>
					<div class="row">
						<div class="col-lg card m-3">
							<input type="hidden" name="eventId" value=<%=event.getEventId() %>/>
							<table class="table">
								<tr>
									<td colspan="2" style="border-top: none;">
										<%for(String hobby : event.getHobbys()){ %>
											<button type="button" class="btn btn-info" disabled><%=hobby%></button>
										<%} %>
									</td>
								</tr>
								<tr>
									<td><h4><%=event.getTitle() %></h4></td>
									<td align="right"><%=event.getCreateUser().getUserName() %></td>
								</tr>
								<tr>
									<td colspan="2"><%=event.getContent() %></td>
								</tr>
								<tr>
									<td colspan="2" align="right">
										<span id="label"><%=event.getCurrentEntry() %> / <%=event.getMaxEntry() %> 人</span>
										<a href="/Tsukutomo/eventDetails/<%= event.getEventId() %>" class="btn btn-primary">詳細</a>
									</td>
								</tr>
							</table>
						</div>
					</div>
				<%}%>
			</div>
			<div class="col-lg"></div>
		</div>
	</div>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
</body>
</html>