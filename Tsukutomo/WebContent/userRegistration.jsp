<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.List"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ツクトモ</title>
	<link rel="stylesheet" href="css/common.css">
	<link rel="stylesheet" href="css/bootstrap.min.css">
</head>
<body>
	<jsp:include page="menu.jsp" flush="true" />
	<%
		ArrayList<String> hobbyList = (ArrayList<String>)request.getAttribute("hobbyList");
		List<String> seletedHobby = (List<String>)request.getAttribute("seleted");
		Integer gender = (Integer)request.getAttribute("gender");
		String error = (String)request.getAttribute("error");
		if (gender == null) gender = 1;
	%>
	<div class="container">
		<div class="row">
			<div class="col-lg"></div>
			<!-- 中央寄せ -->
			<div class="col-lg-6">
				<div class="row">
					<div class="col-lg">
						<h1 class="text-center mb-2">ユーザー登録</h1>
					</div>
				</div>
				<div class="row">
					<%if(!(error == null)){ %>
						<p style="color: red; font-size: larger;"><%=error %></p>
					<% }%>
				</div>
				<div class="row">
					<div class="col-lg">
						<form method="post" name="userRegistration" action="userRegistration">
							<div class="form-group">
								<label for="title">ユーザ名</label>
								<input type="text" class="form-control" name="name" value="${name}" required="required" />
							</div>
							<div class="form-row">
								<div class="form-group col-lg-12">
									<label for="title">性別</label>
								</div>
								<div class="form-group col-lg-4">
									<input type="radio" name="gender" value="1" <% if (gender == 1) out.print("checked=\"checked\""); %>>
									<label>男</label>
								</div>
								<div class="form-group col-lg-4">
									<input type="radio" name="gender" value="2" <% if (gender == 2) out.print("checked=\"checked\""); %>>
									<label>女</label>
								</div>
								<div class="form-group col-lg-4">
									<input type="radio" name="gender" value="9" <% if (gender == 9) out.print("checked=\"checked\""); %>>
									<label>その他</label>
								</div>
							</div>
							<div class="form-group">
								<label for="title">メールアドレス</label>
								<input class="mail form-control" type="email" name="mail" size="100" required="required" value="${mail}">
							</div>
							<div class="form-group">
								<label for="title">パスワード</label>
								<input type="password" class="form-control" name="confirmPassword" pattern="^.{1,64}$" required="required" value="${password}" />
							</div>
							<div class="form-group">
								<label for="title">パスワードの確認</label>
								<input type="password" class="form-control" name="password" pattern="^.{1,64}$" required="required" value="${confirmPassword}" />
							</div>
							<div class="form-group">
								<label for="title">	あなたの趣味</label>
								<div class="container">
									<div class="row">
										<%for(String hobby : hobbyList){ %>
											<div class="col-lg-3">
												<%if(seletedHobby.contains(hobby)){ %>
													<input type="checkbox" name="hobby" checked="checked" value=<%=hobby %>>
												<%}else{ %>
													<input type="checkbox" name="hobby" value=<%=hobby %>>
												<%} %>
												<label><%=hobby %></label>
											</div>
										<%} %>
									</div>
								</div>
							</div>
							<div class="form-group">
								<input class="btn btn-primary btn-lg btn-block" type="submit" value="登録">
							</div>
							<div class="form-group text-center">
								<a href="/Tsukutomo/login">戻る</a>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="col-lg"></div>
		</div>
	</div>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
</body>
</html>