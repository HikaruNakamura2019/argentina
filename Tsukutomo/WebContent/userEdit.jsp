<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="java.util.List"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="dto.EventDto"%>
<%@ page import="dao.EventDao"%>
<%@ page import="dto.UserDto"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>ツクトモ</title>
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/common.css">
</head>
<body>
	<jsp:include page="menu.jsp" flush="true" />
	<%
		String error = (String) request.getAttribute("error");
		List<String> userHobbys = (List<String>)request.getAttribute("userHobbys");
		ArrayList<String> hobbyList = (ArrayList<String>)request.getAttribute("hobbyList");
	%>
	<div class="container">
		<div class="row">
			<div class="col-lg"></div>
			<div class="col-lg-6 container">
				<div class="row">
					<div class="col-lg">
						<h1 class="text-center mb-5">プロフィール編集</h1>
					</div>
				</div>
				<div class="row">
					<div class="col-lg">
						<%if(error != null){ %>
							<p style="color: red; font-size: larger;"><%=error %></p>
						<%}
							request.setAttribute("error", null);
						%>
					</div>
				</div>
				<div class="row">
					<div class="col-lg">
						<form method="post" action="userEdit">
							<div class="form-group">
								<label for="title">パスワード</label>
								<input class="form-control" type="password" name="password" pattern="^.{1,64}$">
							</div>
							<div class="form-group">
								<label for="title">パスワードの確認</label>
								<input class="form-control" type="password" name="confirmationPassword" pattern="^.{1,64}$">
							</div>
							<div class="form-group">
								<label for="title">趣味</label>
								<div class="container">
									<div class="row">
										<%for(String hobby : hobbyList){ %>
											<div class="col-lg-3">
												<%if(userHobbys.contains(hobby)){ %>
													<input type="checkbox" name="hobby" checked="checked" value=<%=hobby %>>
												<%}else{ %>
													<input type="checkbox" name="hobby" value=<%=hobby %>>
												<%} %>
												<label><%=hobby %></label>
											</div>
										<%} %>
									</div>
								</div>
							</div>
							<button type="submit" class="btn btn-primary btn-lg btn-block">編集</button>
							<div class="text-center mt-3">
								<a href="myPage" style="margin-top: 20px;">戻る</a>
							</div>
						</form>
					</div>
				</div>
			</div>
			<div class="col-lg"></div>
		</div>
	</div>

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
</body>
</html>