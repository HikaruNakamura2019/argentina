package page;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.EventDao;
import dao.UserDao;
import dto.EventDto;
import dto.UserDto;

@WebServlet(name="myPage", urlPatterns={"/myPage"})
public class MyPageAction extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		request.setAttribute("error", null);
		UserDao dao = new UserDao();
		//セッション取得
		HttpSession session = request.getSession();
		String view = "/myPage.jsp";

		//ログインしてなかったらログイン画面へ飛ばす
		if(session.getAttribute("user") == null){
		view = "login";
		}else {
			view = "/myPage.jsp";
			//セッションに登録されているユーザのメールアドレスをもとにユーザの情報をDBから取得
			UserDto  user = dao.searchByUserMailAddress(((UserDto) session.getAttribute("user")).getMailAddress());
			String userName=user.getUserName();
			String userGender=genderSelect(user.getGender());
			ArrayList<EventDto> eventList = getEvent(userName);
			List<String> hobbyList = user.getHobbyName() ;
			request.setAttribute("username", userName);
			request.setAttribute("gender", userGender);
			request.setAttribute("hobbyList", hobbyList);
			request.setAttribute("eventList", eventList);
			}
		RequestDispatcher dispatcher = request.getRequestDispatcher(view);
		dispatcher.forward(request, response);
	}

		protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		request.setAttribute("error", null);
		UserDao dao = new UserDao();
		//セッション取得
		HttpSession session = request.getSession();
		//セッションに登録されているユーザのメールアドレスをもとにユーザの情報をDBから取得
		UserDto  user = dao.searchByUserMailAddress(((UserDto) session.getAttribute("user")).getMailAddress());
		String view = "/myPage.jsp";
		String userName=user.getUserName();
		String userGender=genderSelect(user.getGender());
		ArrayList<EventDto> eventList = getEvent(userName);
		List<String> hobbyList = user.getHobbyName() ;
		request.setAttribute("username", userName);
		request.setAttribute("gender", userGender);
		request.setAttribute("hobbyList", hobbyList);
		request.setAttribute("eventList", eventList);
		System.out.println(userName);
		for(EventDto event : eventList) {
			System.out.println(event);
			event.view();
		}
		RequestDispatcher dispatcher = request.getRequestDispatcher(view);
		dispatcher.forward(request, response);
	}

	protected ArrayList<EventDto> getEvent(String name){//イベント一覧取得
		EventDao eventDao = new EventDao();
		ArrayList<EventDto> eventList = eventDao.selectByEntry(name);
		if(!(eventList.isEmpty())) {
		}else {
			System.out.println("参加したイベントはありません。");
		}
		return eventList;
	}
	/*
	 *  get user_id by user_name
	 *  @param String name
	 *  @return strGender
	 */
	protected String genderSelect(int gender) {//genderの数字から「男性」「女性」を分岐
		String strGender="不明";
		if(gender==1) {
			strGender="男性";
		}else if(gender==2) {
			strGender="女性";
		}else {
			strGender="その他";
		}
		return strGender;
	}




}
