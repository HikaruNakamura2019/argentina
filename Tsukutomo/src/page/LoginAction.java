package page;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import dto.UserDto;

@WebServlet(name = "login", urlPatterns = { "/login" })
public class LoginAction extends HttpServlet {
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

		//HttpServletRequestに格納されているHTMLから送信されたdoPostメソッドの第一引数を受け取る

		req.getRequestDispatcher("login.jsp").forward(req, res);
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {

		//HttpServletRequestに格納されているHTMLから送信されたdoPostメソッドの第一引数を受け取る

		String mail = req.getParameter("mail");
		String pass = req.getParameter("pass");
		req.setAttribute("exampleInputEmail1", mail);
		req.setAttribute("exampleInputPassword1", pass);

		UserDao dao = new UserDao();
		UserDto user = dao.searchByUserMailAddress(mail);

		boolean isLogin = (user != null && mail.equals(user.getMailAddress()) &&
				pass.equals(user.getPassword()));

		if (isLogin) {
			HttpSession session = req.getSession();
			user.setPassword("");
			session.setAttribute("user", user);
			res.sendRedirect("index");
		} else {
			req.setAttribute("error", "メールアドレスかパスワードが間違っています。");
			req.getRequestDispatcher("login.jsp").forward(req, res);
		}
	}
}
