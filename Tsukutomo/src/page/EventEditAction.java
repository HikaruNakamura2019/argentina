package page;

import java.io.IOException;
//import java.security.Timestamp;使わない？
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.EventDao;
import dao.HobbyDao;
import dto.EventDto;
import dto.UserDto;

@WebServlet(name="eventEdit", urlPatterns={"/eventEdit/*"})
public class EventEditAction extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String path = request.getPathInfo();
		HttpSession session = request.getSession();

		UserDto user = ((UserDto)session.getAttribute("user"));

		if (user == null) {
			response.sendRedirect("/Tsukutomo/login");
		} else if (path == null) {
			response.sendRedirect("/Tsukutomo/index");
		}  else {
			int eventId = -1;
			try {
				eventId = Integer.parseInt(path.replace("/", ""));
			} catch (Exception e) {
				response.sendRedirect("/Tsukutomo/index");
			}

			if (eventId != -1) {
				EventDao eventDao = new EventDao();
				EventDto event = eventDao.selectById(eventId);

				if (user.getUserId() == event.getCreateUser().getUserId()) {

					Timestamp timestamp = event.getEventDate();

					HobbyDao hobbyDao = new HobbyDao();
					ArrayList<String> hobbyNameList = hobbyDao.selectAllHobbyName();
					request.setAttribute("hobbyList", hobbyNameList);
					request.setAttribute("eventTitle", event.getTitle());
					request.setAttribute("maxEntry", event.getMaxEntry());
					request.setAttribute("eventDate", new SimpleDateFormat("yyyy-MM-dd").format(timestamp));
					request.setAttribute("eventHour", new SimpleDateFormat("HH").format(timestamp));
					request.setAttribute("eventMin", new SimpleDateFormat("mm").format(timestamp));
					request.setAttribute("eventHobbys", event.getHobbys());
					request.setAttribute("eventContent", event.getContent());
					request.setAttribute("eventId", event.getEventId());

					RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/eventEdit.jsp");
					dispatcher.forward(request, response);
				} else {
					response.sendRedirect("/Tsukutomo/index");
				}
			}
		}

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		String eventId = request.getParameter("eventId");
		String title = request.getParameter("title");
		String max_entry = request.getParameter("max_entry");
		String[] hobbys = request.getParameterValues("hobby");
		String date = request.getParameter("date");
		String hour = request.getParameter("hour");
		String min = request.getParameter("min");
		String content = request.getParameter("content");

		EventDto event = new EventDto();
		EventDao eventDao = new EventDao();
		event.setEventId(Integer.parseInt(eventId));
		event.setTitle(title);
		event.setMaxEntry(Integer.parseInt(max_entry));
		event.setHobbys(Arrays.asList(hobbys));
		try {
			event.setEventDate(new Timestamp(new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(date + " " + hour + ":" + min).getTime()));
		} catch (ParseException e) {
			e.printStackTrace();
		}
		event.setContent(content);
		event.setEntryUsersName(null);

		eventDao.updateEvent(event);

		response.sendRedirect("/Tsukutomo/eventDetails/" + eventId);
	}
}
