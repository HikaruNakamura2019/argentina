package page;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.EventDao;
import dao.HobbyDao;
import dto.EventDto;
import dto.UserDto;

@WebServlet(name = "eventRegistration", urlPatterns = { "/eventRegistration" })
public class EventRegistrationAction extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		UserDto user = (UserDto) session.getAttribute("user");



		if (user == null) {
			response.sendRedirect("/Tsukutomo/login");
		} else {
			request.setAttribute("hobbyList", "");
			request.setAttribute("eventName", "");
			request.setAttribute("startDate", "");
			request.setAttribute("hour", "");
			request.setAttribute("min", "");
			request.setAttribute("peopleRestriction", "");
			request.setAttribute("text", "");
			HobbyDao hobbyDao = new HobbyDao();
			ArrayList<String> hobbyNameList = hobbyDao.selectAllHobbyName();
			request.setAttribute("hobbyList", hobbyNameList);
			String view = "/eventRegistration.jsp";
			RequestDispatcher dispatcher = request.getRequestDispatcher(view);
			dispatcher.forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		req.setCharacterEncoding("UTF-8");

		HttpSession session = req.getSession();

		//リクエストパラメーターの取得
		String eventName = req.getParameter("eventName");
		int peopleRestriction = Integer.valueOf(req.getParameter("peopleRestriction"));
		String startDate = req.getParameter("startDate");
		System.out.println(startDate);
		String hour = req.getParameter("hour");
		String min = req.getParameter("min");
		String[] hobbyTag = req.getParameterValues("hobby");
		String text = req.getParameter("text");

		String error = "";
		if (eventName == null || hour == null || min == null || startDate == null || hobbyTag == null || text == null) {
			if (eventName == null)  error = "イベント名が未入力です。";
			if (hour == null)       error = "締め切り(時)を入力してください";
			if (min == null)        error = "締め切り(分)を入力して下さい";
			if (startDate == null)  error = "締め切り日を入してください";
			if (hobbyTag == null)   error = "趣味タグを登録してください";
			if (text == null)       error = "本文が空白です";


			HobbyDao hobbyDao = new HobbyDao();
			ArrayList<String> hobbyNameList = hobbyDao.selectAllHobbyName();
			req.setAttribute("hobbyList", hobbyNameList);
			req.setAttribute("eventName", eventName);
			req.setAttribute("startDate", startDate);
			req.setAttribute("hour", hour);
			req.setAttribute("min", min);
			req.setAttribute("peopleRestriction", peopleRestriction);
			req.setAttribute("text", text);

			req.setAttribute("error", error);
			req.getRequestDispatcher("eventRegistration.jsp").forward(req, res);
		} else {
			List<String> hobbys = new ArrayList<String>();
			for (String hobby : hobbyTag) {
				hobbys.add(hobby);
			}

			//登録するイベント情報の設定
			EventDao dao = new EventDao();
			EventDto event = new EventDto();
			Timestamp hogeTime = new Timestamp(System.currentTimeMillis());
			UserDto user = (UserDto) session.getAttribute("user");
			boolean eventAction = (eventName != null);

			if (eventAction) {
				try {
					String time = startDate + "-" + hour + "-" + min;
					SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd-HH-mm");
					hogeTime = new Timestamp(sdf.parse(time).getTime());
				} catch (Exception e) {
					e.printStackTrace();
				}
				event.setCreateUser(user);
				List<String> UserNames = new ArrayList<String>();
				UserNames.add(user.getUserName());
				event.setEntryUsersName(UserNames);
				event.setHobbys(hobbys);
				event.setTitle(eventName);
				event.setMaxEntry(peopleRestriction);
				event.setEventDate(hogeTime);
				event.setContent(text);
				dao.insertEvent(event);
				int eventId = getEventIdByUserName(user.getUserName());
				session = req.getSession();
				session.setAttribute("user", user);
				req.setAttribute("Id", eventId);
				res.sendRedirect("/Tsukutomo/eventDetails/" + eventId);
			}
		}
	}

	public int getEventIdByUserName(String name) {
		int maxId = -1;
		EventDao eventDao = new EventDao();
		ArrayList<EventDto> events = eventDao.selectByEntry(name);
		for (EventDto event : events) {
			int id = event.getEventId();
			if (maxId < id) {
				maxId = id;
			}
		}
		return maxId;
	}
}
