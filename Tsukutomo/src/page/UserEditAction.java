package page;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.HobbyDao;
import dao.UserDao;
import dao.UserHobbyDao;
import dto.UserDto;

@WebServlet(name="userEdit", urlPatterns={"/userEdit"})
public class UserEditAction extends HttpServlet {

	//userEdit.jspに遷移
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		request.setAttribute("error", null);
	    UserHobbyDao userHobbyDao = new UserHobbyDao();
	    HttpSession session = request.getSession();
	    String view = "userEdit.jsp";

	    //ログインしてなかったらログイン画面へ飛ばす
	    if(session.getAttribute("user") == null){
	    	view = "login";
	    }else {
	    	UserDto user = (UserDto) session.getAttribute("user");
	    	List<String> userHobbys = userHobbyDao.searchHobbyNamesByUserId(user.getUserId());
	    	HobbyDao hobbyDao = new HobbyDao();
	    	ArrayList<String> hobbyNameList = hobbyDao.selectAllHobbyName();

	    	request.setAttribute("user",user);
	    	request.setAttribute("userHobbys", userHobbys);
			request.setAttribute("hobbyList", hobbyNameList);

	    }
	    RequestDispatcher dispatcher = request.getRequestDispatcher(view);
	    dispatcher.forward(request, response);
	}

	//
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");
		//ユーザーIDを取得
		HttpSession session = request.getSession();
		int user_id = ((UserDto)session.getAttribute("user")).getUserId();
	    UserHobbyDao userHobbyDao = new UserHobbyDao();
	    HobbyDao hobbyDao = new HobbyDao();
	    UserDto user = (UserDto) session.getAttribute("user");

    	List<String> userHobbys = userHobbyDao.searchHobbyNamesByUserId(user.getUserId());
    	ArrayList<String> hobbyNameList = hobbyDao.selectAllHobbyName();
		request.setAttribute("hobbyList", hobbyNameList);
    	request.setAttribute("userHobbys", userHobbys);


		String cpw = request.getParameter("confirmationPassword");
		String[] selectedHobbys = request.getParameterValues("hobby");
		String pw = request.getParameter("password");
		//パスワードが入力されていなかったらnullを入れる
		if(pw == "") {
			pw = null;
		}

		System.out.println(pw);
		System.out.println(cpw);

		String view = "userEdit.jsp";
	    request.setAttribute("user",(UserDto)session.getAttribute("user"));
		//入力値チェック
	   if((pw != null) && !(pw.equals(cpw))) {
			request.setAttribute("error", "パスワードと確認用パスワードが一致しません。");
		}else if(selectedHobbys == null) {
			request.setAttribute("error", "趣味は一つ以上選んでください");
		}else {
			//問題なければ入力値を登録して、マイページへ遷移
			view = "myPage";
			//趣味をListに格納
			List<String> hobbys = new ArrayList<String>();
			for (String hobby : selectedHobbys) {
				System.out.println(hobby);
				hobbys.add(hobby);
			}

			//データベースに登録
			UserDao userDao = new UserDao();
			userDao.update(user_id, pw, hobbys);
		}

		RequestDispatcher dispatcher = request.getRequestDispatcher(view);
		dispatcher.forward(request, response);
	}
}
