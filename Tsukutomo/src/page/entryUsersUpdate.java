package page;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.EventDao;
import dto.EventDto;
import dto.UserDto;

/** イベントへの参加および辞退を制御
 * @author AZUMA Ginji
 */

@WebServlet(name = "entryUsersUpdate", urlPatterns = { "/entryUsersUpdate" })
public class entryUsersUpdate extends HttpServlet {
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		HttpSession session = req.getSession();
		UserDto user = (UserDto) session.getAttribute("user");
		int eventId = Integer.parseInt(req.getParameter("eventId"));
		String action = req.getParameter("action"); // 参加 or 辞退
		String nextPage = "eventDetails/" + eventId;

		EventDao eventDao = new EventDao();
		EventDto event = eventDao.selectById(eventId);
		List<String> entrys = event.getEntryUsersName();
		if (action.equals("join")) {
			entrys.add(user.getUserName());
		} else if (action.equals("decline")) {
			entrys.remove(entrys.indexOf(user.getUserName()));
		} else {
			System.out.println("不正な画面遷移を検知しました");
			nextPage = "index";
		}
		event.setEntryUsersName(entrys);
		eventDao.updateEvent(event);

		res.sendRedirect(nextPage);
	}
}
