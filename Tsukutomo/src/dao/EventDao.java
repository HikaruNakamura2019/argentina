package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;

import dto.EventDto;

/** EventDao
 * @author AZUMA Ginji
 * @version 2019/05/23
 */
public class EventDao {
	// ローカルでのテスト環境か
	static final boolean isTest = true;
	// DB 設定
	static final String DB = isTest ? "jdbc:mysql://127.0.0.1:3306/tsukutomo_db?serverTimezone=JST" : "jdbc:mysql://127.0.0.1:3306/tsukutomo_db?serverTimezone=JST";
	static final String USER = isTest ? "tsukutomo" : "tsukutomo";
	static final String PASS = isTest ? "tsukutsukutomotomo" : "Tsu92ku10MOtomo?";
	// 各テーブルおよび view 名
	static final String VIEW_EVENT = "event_view";
	static final String VIEW_EVENT_HOBBY = "event_hobby_view";
	static final String VIEW_EVENT_ENTRY_USER = "event_entry_user_view";
	static final String EVENT_TB = "event";
	static final String EVENT_HOBBY_TB = "event_hobby";
	static final String EVENT_ENTRY_USER_TB = "event_entry_user";

	// イベントの検索結果を返す
	protected EventDto rowMappingEvent(ResultSet rs) throws SQLException {
		EventDto event = new EventDto();
		UserDao userDao = new UserDao();
		event.setEventId(rs.getInt("id"));
//		event.setEventId(1);
		event.setCreateUser(userDao.searchByUserName(rs.getString("name")));
		event.setTitle(rs.getString("title"));
		event.setContent(rs.getString("content"));
		event.setCurrentEntry(rs.getInt("current_entry"));
		event.setMaxEntry(rs.getInt("max_entry"));
		event.setEventDate(rs.getTimestamp("event_date"));
		return event;
	}

	// イベントと趣味の対応を返す
	protected EventHobby rowMappingEventHobby(ResultSet rs) throws SQLException {
		EventHobby eventHobby = new EventHobby();
		eventHobby.setEventId(rs.getInt("id"));
		eventHobby.setHobby(rs.getString("name"));
		return eventHobby;
	}

	// イベントと参加しているユーザの対応を返す
	protected EventEntryUser rowMappingEventEntryUser(ResultSet rs) throws SQLException {
		EventEntryUser eventEntryUser = new EventEntryUser();
		eventEntryUser.setEventId(rs.getInt("id"));
		eventEntryUser.setUserName(rs.getString("name"));
		return eventEntryUser;
	}

	/** イベント ID によるイベントの検索
	 * @param eventId
	 * @return Event エントリ
	 */
	public EventDto selectById(int eventId) {
		EventDto event = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection con = DriverManager.getConnection(DB, USER, PASS);

			// 基本イベント情報の取得
			StringBuilder sb = new StringBuilder("SELECT * FROM " + VIEW_EVENT + " WHERE id = ?");
			PreparedStatement stmt = con.prepareStatement(sb.toString());
			stmt.setInt(1, eventId);
			ResultSet rs = stmt.executeQuery();

			event = new EventDto();
			while (rs.next()) {
				event = rowMappingEvent(rs);
			}

			// イベントに対応する趣味を取得
			event.setHobbys(hobbySelectById(event.getEventId()));
			// イベントに参加応募しているユーザ一覧を取得
			event.setEntryUsersName(entrySelectById(event.getEventId()));

			stmt.close();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return event;
	}

	/** 趣味によるイベントの検索
	 * @param hobby
	 * @return Event エントリの ArrayList
	 */
	public ArrayList<EventDto> selectByHobby(String hobby) {
		ArrayList<EventDto> events = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection con = DriverManager.getConnection(DB, USER, PASS);

			// 趣味に対応するイベントを取得
			StringBuilder sb = new StringBuilder("SELECT * FROM " + VIEW_EVENT_HOBBY + " WHERE name = ?");
			PreparedStatement stmt = con.prepareStatement(sb.toString());
			stmt.setString(1, hobby);
			ResultSet rs = stmt.executeQuery();
			ArrayList<EventHobby> targetEvents = new ArrayList<>();
			while (rs.next()) {
				targetEvents.add(rowMappingEventHobby(rs));
			}

			events = new ArrayList<EventDto>();
			// 各イベントの DTO を生成
			for (EventHobby event : targetEvents) {
				events.add(selectById(event.getEventId()));
			}

			stmt.close();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return events;
	}

	/** ユーザが参加しているイベント一覧を取得
	 * @param userName
	 * @return ユーザが参加しているイベント一覧
	 */
	public ArrayList<EventDto> selectByEntry(String userName) {
		ArrayList<EventDto> events = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection con = DriverManager.getConnection(DB, USER, PASS);

			// ユーザが参加しているイベントを取得
			StringBuilder sb = new StringBuilder("SELECT * FROM " + VIEW_EVENT_ENTRY_USER + " WHERE name = ?");
			PreparedStatement stmt = con.prepareStatement(sb.toString());
			stmt.setString(1, userName);
			ResultSet rs = stmt.executeQuery();
			ArrayList<EventEntryUser> entryEvents = new ArrayList<>();
			while (rs.next()) {
				entryEvents.add(rowMappingEventEntryUser(rs));
			}

			events = new ArrayList<EventDto>();
			// 各イベントの DTO を生成
			for (EventEntryUser event : entryEvents) {
				events.add(selectById(event.getEventId()));
			}

			stmt.close();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return events;
	}

	/** 開催日時によるイベントの検索
	 * oldTime ~ nextTime の間のイベントを検索
	 * @param oldTime
	 * @param nextTime
	 * @return Event エントリの ArrayList
	 */
	public ArrayList<EventDto> selectByTime(Timestamp oldTime, Timestamp nextTime) {
		ArrayList<EventDto> events = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection con = DriverManager.getConnection(DB, USER, PASS);

			// 基本イベント情報の取得
			StringBuilder sb = new StringBuilder("SELECT * FROM " + VIEW_EVENT);
			sb.append(" WHERE ? <= event_date AND event_date <= ?");
			PreparedStatement stmt = con.prepareStatement(sb.toString());
			stmt.setTimestamp(1, oldTime);
			stmt.setTimestamp(2, nextTime);
//			System.out.println(stmt); // SQL 文のテストプリント
			ResultSet rs = stmt.executeQuery();

			events = new ArrayList<>();
			while (rs.next()) {
				events.add(rowMappingEvent(rs));
			}

			for (EventDto event : events) {
				// イベントに対応する趣味を取得
				event.setHobbys(hobbySelectById(event.getEventId()));
				// イベントに参加応募しているユーザ一覧を取得
				event.setEntryUsersName(entrySelectById(event.getEventId()));
			}

			stmt.close();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return events;
	}

	/** 指定日付範囲内のイベントを趣味で絞って検索
	 * @param hobby
	 * @param oldTime
	 * @param nextTime
	 * @return Event エントリの ArrayList
	 */
	public ArrayList<EventDto> searchEvent(String hobby, Timestamp oldTime, Timestamp nextTime) {
		ArrayList<EventDto> events = null;
		events = selectByTime(oldTime, nextTime);
		events.removeIf(event -> !event.getHobbys().contains(hobby));
		return events;
	}

	/** 全イベント情報の検索
	 * @return 全 Event エントリの ArrayList
	 */
	public ArrayList<EventDto> selectAll() {
		ArrayList<EventDto> events = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection con = DriverManager.getConnection(DB, USER, PASS);

			// 基本イベント情報の取得
			StringBuilder sb = new StringBuilder("SELECT * FROM " + VIEW_EVENT);
			PreparedStatement stmt = con.prepareStatement(sb.toString());
			ResultSet rs = stmt.executeQuery();

			events = new ArrayList<>();
			while (rs.next()) {
				events.add(rowMappingEvent(rs));
			}

			for (EventDto event : events) {
				// イベントに対応する趣味を取得
				event.setHobbys(hobbySelectById(event.getEventId()));
				// イベントに参加応募しているユーザ一覧を取得
				event.setEntryUsersName(entrySelectById(event.getEventId()));
			}

			stmt.close();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return events;
	}

	/** イベントに対応する趣味を検索
	 * @param eventId
	 * @return イベントに対応する趣味一覧
	 */
	public ArrayList<String> hobbySelectById(int eventId) {
		ArrayList<String> hobbys = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection con = DriverManager.getConnection(DB, USER, PASS);

			StringBuilder sb = new StringBuilder("SELECT * FROM " + VIEW_EVENT_HOBBY + " WHERE id = ?");
			PreparedStatement stmt = con.prepareStatement(sb.toString());
			stmt.setInt(1, eventId);
			ResultSet rs = stmt.executeQuery();
			ArrayList<EventHobby> eventHobbys = new ArrayList<>();
			while (rs.next()) {
				eventHobbys.add(rowMappingEventHobby(rs));
			}
			hobbys = new ArrayList<>();
			for (EventHobby hobby : eventHobbys) {
				if (hobby.getEventId() == eventId) {
					hobbys.add(hobby.getHobby());
				}
			}
			stmt.close();
			con.close();
		} catch  (Exception e) {
			e.printStackTrace();
		}
		return hobbys;
	}

	/** イベントに参加しているユーザ一覧を取得
	 * @param eventId
	 * @return イベントに参加しているユーザ一覧
	 */
	public ArrayList<String> entrySelectById(int eventId) {
		ArrayList<String> users = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection con = DriverManager.getConnection(DB, USER, PASS);

			StringBuilder sb = new StringBuilder("SELECT * FROM " + VIEW_EVENT_ENTRY_USER + " WHERE id = ?");
			PreparedStatement stmt = con.prepareStatement(sb.toString());
			stmt.setInt(1, eventId);
			ResultSet rs = stmt.executeQuery();
			ArrayList<EventEntryUser> entryUsers = new ArrayList<>();
			while (rs.next()) {
				entryUsers.add(rowMappingEventEntryUser(rs));
			}
			users = new ArrayList<>();
			for (EventEntryUser user : entryUsers) {
				if (user.getEventId() == eventId) {
					users.add(user.getUserName());
				}
			}
			con.close();
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return users;
	}

	/** イベント情報を登録
	 * @param event 登録するイベント情報
	 */
	public void insertEvent(EventDto event) {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection con = DriverManager.getConnection(DB, USER, PASS);

			// event テーブルに登録
			StringBuilder sb = new StringBuilder("INSERT INTO " + EVENT_TB);
			sb.append(" (user_id, title, content, max_entry, event_date) ");
			sb.append(" VALUES (?, ?, ?, ?, ?)");
			PreparedStatement stmt = con.prepareStatement(sb.toString());
			stmt.setInt(1,  event.getCreateUser().getUserId());
			stmt.setString(2, event.getTitle());
			stmt.setString(3, event.getContent());
			stmt.setInt(4, event.getMaxEntry());
			stmt.setTimestamp(5, event.getEventDate());
			stmt.executeUpdate();

			// 登録したイベントの ID を取得
			int registerEventId = -1;
			sb = new StringBuilder("SELECT MAX(id) FROM " + EVENT_TB);
			stmt = con.prepareStatement(sb.toString());
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				registerEventId = rs.getInt("max(id)");
			}
			// event_hobby テーブルに登録
			HobbyDao hobbyDao = new HobbyDao();
			for (String hobby : event.getHobbys()) {
				sb = new StringBuilder("INSERT INTO " + EVENT_HOBBY_TB);
				sb.append(" (event_id, hobby_id) VALUES (?, ?)");
				stmt = con.prepareStatement(sb.toString());
				stmt.setInt(1, registerEventId);
				stmt.setInt(2,  hobbyDao.searchHobbyIdByHobbyName(hobby)); // TODO -1 が帰った場合、エラーを投げる
//				System.out.println(stmt); // テストプリント
				stmt.executeUpdate();
			}

			// event_entry_user テーブルに登録
			sb = new StringBuilder("INSERT INTO " + EVENT_ENTRY_USER_TB);
			sb.append(" (event_id, user_id) VALUES (?, ?)");
			stmt = con.prepareStatement(sb.toString());
			stmt.setInt(1, registerEventId);
			stmt.setInt(2,  event.getCreateUser().getUserId()); // TODO -1 が帰った場合、エラーを投げる
//			System.out.println(stmt); // テストプリント
			stmt.executeUpdate();

			stmt.close();
			con.close();
		} catch  (Exception e) {
			e.printStackTrace();
		}
	}

	/** イベント ID に一致するイベント情報を更新
	 * @param event
	 */
	public void updateEvent(EventDto event) {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection con = DriverManager.getConnection(DB, USER, PASS);

			// 古いイベント情報を取得
			EventDto oldEvent = selectById(event.getEventId());
			// event テーブルを更新
			StringBuilder sb = new StringBuilder("UPDATE " + EVENT_TB);
			sb.append(" SET ");
			sb.append("   title = ?, ");
			sb.append("   content = ?, ");
			sb.append("   max_entry = ?, ");
			sb.append("   event_date = ? ");
			sb.append(" WHERE id = ?");
			PreparedStatement stmt = con.prepareStatement(sb.toString());
			stmt.setString(1, (event.getTitle() != null) ? event.getTitle() : oldEvent.getTitle());
			stmt.setString(2, (event.getContent() != null) ? event.getContent() : oldEvent.getContent());
			stmt.setInt(3, (event.getMaxEntry() != -1) ? event.getMaxEntry() : oldEvent.getMaxEntry());
			stmt.setTimestamp(4, (event.getEventDate() != null) ? event.getEventDate() : oldEvent.getEventDate());
			stmt.setInt(5, (event.getEventDate() != null) ? event.getEventId() : oldEvent.getEventId());
			stmt.executeUpdate();

			// event_hobby テーブルを更新
			if (event.getHobbys() != null) {
				sb = new StringBuilder("DELETE FROM " + EVENT_HOBBY_TB + " WHERE event_id = ?");
				stmt = con.prepareStatement(sb.toString());
				stmt.setInt(1, event.getEventId());
//				System.out.println(stmt.toString());  // テストプリント
				stmt.executeUpdate();
				HobbyDao hobbyDao = new HobbyDao();
				for (String hobby : event.getHobbys()) {
					sb = new StringBuilder("INSERT INTO " + EVENT_HOBBY_TB);
					sb.append(" (event_id, hobby_id) VALUES (?, ?)");
					stmt = con.prepareStatement(sb.toString());
					stmt.setInt(1, event.getEventId());
					stmt.setInt(2,  hobbyDao.searchHobbyIdByHobbyName(hobby)); // TODO -1 が帰った場合、エラーを投げる
//					System.out.println(stmt.toString());  // テストプリント
					stmt.executeUpdate();
				}
			}

			// event_entry_user テーブルに更新
			if (event.getEntryUsersName() != null) {
				sb = new StringBuilder("DELETE  FROM " + EVENT_ENTRY_USER_TB + " WHERE event_id = ?");
				stmt = con.prepareStatement(sb.toString());
				stmt.setInt(1, event.getEventId());
				stmt.executeUpdate();
				for (String userName : event.getEntryUsersName() ) {
					UserDao userDao = new UserDao();
					sb = new StringBuilder("INSERT INTO " + EVENT_ENTRY_USER_TB);
					sb.append(" (event_id, user_id) VALUES (?, ?)");
					stmt = con.prepareStatement(sb.toString());
					stmt.setInt(1, event.getEventId());
					stmt.setInt(2,  userDao.getUserId(userName)); // TODO -1 が帰った場合、エラーを投げる
					stmt.executeUpdate();
				}
			}

			stmt.close();
			con.close();
		} catch  (Exception e) {
			e.printStackTrace();
		}
	}

	// イベントに対応する趣味
	public static class EventHobby {
		private int eventId;
		private String hobby;

		public int getEventId() {
			return eventId;
		}

		public void setEventId(int id) {
			this.eventId = id;
		}

		public String getHobby() {
			return hobby;
		}

		public void setHobby(String hobby) {
			this.hobby = hobby;
		}
	}

	// イベントに参加しているユーザ
	public static class EventEntryUser {
		private int eventId;
		private String userName;

		public int getEventId() {
			return this.eventId;
		}

		public void setEventId(int id) {
			this.eventId = id;
		}

		public String getUserName() {
			return this.userName;
		}

		public void setUserName(String name) {
			this.userName = name;
		}
	}
}