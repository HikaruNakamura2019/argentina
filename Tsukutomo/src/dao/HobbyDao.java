package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;

public class HobbyDao {
	// ローカルでのテスト環境か
	static final boolean isTest = true;
	// DB 設定
	static final String DB = isTest ? "jdbc:mysql://127.0.0.1:3306/tsukutomo_db?serverTimezone=JST" : "jdbc:mysql://127.0.0.1:3306/tsukutomo_db?serverTimezone=JST";
	static final String USER = isTest ? "tsukutomo" : "tsukutomo";
	static final String PASS = isTest ? "tsukutsukutomotomo" : "Tsu92ku10MOtomo?";
	// 各テーブル名
	static final String HOBBY_TB = "hobby";

	/** 全検索
	 * @return 全エントリ
	 */
	public HashMap<Integer, String> selectAll() {
		HashMap<Integer, String> hobbys = new HashMap<Integer, String>();
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection con = DriverManager.getConnection(DB, USER, PASS);
			// 全検索
			StringBuilder sb = new StringBuilder("SELECT * FROM " + HOBBY_TB);
			PreparedStatement stmt = con.prepareStatement(sb.toString());
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				hobbys.put(rs.getInt("id"), rs.getString("name"));
			}
			stmt.close();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return hobbys;
	}

	public ArrayList<String> selectAllHobbyName() {
		ArrayList<String> hobbys = new ArrayList<>();
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection con = DriverManager.getConnection(DB, USER, PASS);
			StringBuilder sb = new StringBuilder("SELECT name FROM " + HOBBY_TB);
			PreparedStatement stmt = con.prepareStatement(sb.toString());
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				hobbys.add(rs.getString("name"));
			}
			stmt.close();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return hobbys;
	}

	/** hobby_id検索
	 * @param hobby_name
	 * @return hobby_id
	 */
	public int searchHobbyIdByHobbyName(String hobby_name) {
		int hobby_id = -1;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			Connection con = DriverManager.getConnection(DB, USER, PASS);
			StringBuilder sb = new StringBuilder("select id from " + HOBBY_TB + " where name = ?");
			PreparedStatement stmt = con.prepareStatement(sb.toString());
			stmt.setString(1, hobby_name);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				hobby_id = rs.getInt("id");
			}
			stmt.close();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return hobby_id;
	}
}
