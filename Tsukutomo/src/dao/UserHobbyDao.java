package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserHobbyDao {
	// ローカルでのテスト環境か
	static final boolean isTest = true;
	// DB options
	private static final String DB_HOST = isTest ? "127.0.0.1" : "127.0.0.1" ;
	private static final String DB_NAME = "tsukutomo_db";
	private static final String DB_USER = "tsukutomo";
	private static final String DB_PASS = isTest ? "tsukutsukutomotomo" : "Tsu92ku10MOtomo?" ;

	private static final String DBMS = "mysql";
	private static final String DB_DRIVE = "con." + DBMS + ".cj.jdbc.Drive";
	private static final String DB_PORT = "3306";
	private static final String DB_URL = "jdbc:" + DBMS + "://" + DB_HOST + ":" + DB_PORT + "/" + DB_NAME + "?serverTimezone=JST";

	// user_hoby table of column name
	private static final String USER_HOBBY_TABLE_NAME = "user_hobby";
	private static final String USER_HOBBY_USER_ID = "user_id";
	private static final String USER_HOBBY_HOBBY_ID = "hobby_id";

	// user_hobby_view of column name
	private static final String USER_HOBBY_VIEW_NAME = "user_hobby_view";
	private static final String USER_HOBBY_USER_NAME = "user_name";
	private static final String USER_HOBBY_HOBBY_NAME = "hobby_name";

	/*
	private Connection con;
	private PreparedStatement stmt;
	private ResultSet rs;

	public UserHobbyDao() {
		try {
			this.con = this.getConnection();
		} catch (ClassNotFoundException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
	}*/

	/*
	 * search hobby name by user id
	 * @param int user id
	 * @return List<String> hobby names
	 */
	public List<String> searchHobbyNamesByUserId(int user_id) {
		List<String> hobby_name_list = new ArrayList<String>();
		StringBuilder sql = new StringBuilder();
		sql.append("select * from " + USER_HOBBY_VIEW_NAME + " where id = ?");
		try {
			Connection con = this.getConnection();
			PreparedStatement stmt;
			ResultSet rs;
			stmt = con.prepareStatement(sql.toString());
			stmt.setString(1, String.valueOf(user_id));
			rs = stmt.executeQuery();
			while (rs.next()) {
				hobby_name_list.add(rs.getString(USER_HOBBY_HOBBY_NAME));
			}
			this.closeStatement(stmt);
			this.closeConnection(con);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return hobby_name_list;
	}

	/*
	 * update hobby of user
	 * @param int user id, String hobby name
	 */
	public void updateHobbyNameOfUser(int user_id, List<String> hobby_names) {
		this.deleteHobbyOfUser(user_id);
		StringBuilder sql = new StringBuilder();
		sql.append("insert into " + USER_HOBBY_TABLE_NAME + " values (?, ?)");
		try {
			Connection con = this.getConnection();
			PreparedStatement stmt = null;
			for (String name : hobby_names) {
				stmt = con.prepareStatement(sql.toString());
				stmt.setString(1, String.valueOf(user_id));
				stmt.setString(2,
						String.valueOf(new HobbyDao().searchHobbyIdByHobbyName(name)));
				stmt.executeUpdate();
			}
			this.closeStatement(stmt);
			this.closeConnection(con);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * insert hobby of user
	 * @param int user id, List<String> hobby_names
	 */
	public void insert(int user_id, List<String> hobby_names) {
		StringBuilder sql = new StringBuilder();
		sql.append("insert into " + USER_HOBBY_TABLE_NAME + " values (?, ?)");
		try {
			Connection con = this.getConnection();
			PreparedStatement stmt = null;
			for (String name : hobby_names) {
				stmt = con.prepareStatement(sql.toString());
				stmt.setInt(1, user_id);
				stmt.setInt(2, new HobbyDao().searchHobbyIdByHobbyName(name));
				stmt.executeUpdate();
			}
			this.closeStatement(stmt);
			this.closeConnection(con);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/*
	 * delete hobby of user
	 * @param int user id
	 */
	public void deleteHobbyOfUser(int user_id) {
		StringBuilder sql = new StringBuilder();
		sql.append("delete from " + USER_HOBBY_TABLE_NAME + " where user_id = ?");
		try {
			Connection con = this.getConnection();
			PreparedStatement stmt;
			stmt = con.prepareStatement(sql.toString());
			stmt.setString(1, String.valueOf(user_id));
			stmt.executeUpdate();
			this.closeStatement(stmt);
			this.closeConnection(con);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// get DB connection
	public Connection getConnection() throws ClassNotFoundException, SQLException {
		Connection con = null;
		if (con == null || con.isClosed()) {
			Class.forName("com.mysql.cj.jdbc.Driver");
			con = DriverManager.getConnection(DB_URL, DB_USER, DB_PASS);
		}
		return con;
	}

	// close DB statement
	public void closeStatement(PreparedStatement stmt) throws SQLException {
		if (stmt != null)
			stmt.close();
	}

	// close DB connection;
	public void closeConnection(Connection con) throws SQLException {
		if (con != null)
			con.close();
	}
}
