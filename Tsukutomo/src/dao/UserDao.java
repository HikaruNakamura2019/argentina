package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import dto.UserDto;

public class UserDao {
	// ローカルでのテスト環境か
	static final boolean isTest = true;
	// DB options
	private static final String DB_HOST = isTest ? "127.0.0.1" : "127.0.0.1" ;
	private static final String DB_NAME = "tsukutomo_db";
	private static final String DB_USER = "tsukutomo";
	private static final String DB_PASS = isTest ? "tsukutsukutomotomo" : "Tsu92ku10MOtomo?" ;

	private static final String DBMS = "mysql";
	private static final String DB_DRIVE = "con." + DBMS + ".cj.jdbc.Drive";
	private static final String DB_PORT = "3306";
	private static final String DB_URL = "jdbc:" + DBMS + "://" + DB_HOST + ":" + DB_PORT + "/" + DB_NAME + "?serverTimezone=JST";

	// user table of column name
	private static final String USER_TABLE_NAME = "user";
	private static final String USER_ID = "id";
	private static final String USER_NAME = "name";
	private static final String USER_MAIL_ADDRESS = "mail_address";
	private static final String USER_PASSWORD = "password";
	private static final String USER_GENDER = "gender";
	private static final String USER_CREATE_DATE = "create_date";

	/*
	 *  search user by user_id
	 *  @param String name
	 *  @return userDTO
	 */
	public UserDto searchByUserId(int userId) {
		UserDto userDto = null;
		StringBuilder sql = new StringBuilder();
		sql.append("select * from " + USER_TABLE_NAME + " where id = ?");
		try {
			Connection con = this.getConnection();
			PreparedStatement stmt;
			ResultSet rs;

			stmt = con.prepareStatement(sql.toString());
			stmt.setInt(1, userId);
			rs = stmt.executeQuery();
			while (rs.next()) {
				userDto = setUserDto(rs);
			}
			this.closeStatement(stmt);
			this.closeConnection(con);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return userDto;
	}

	/*
	 *  search user by user_name
	 *  @param String name
	 *  @return userDTO
	 */
	public UserDto searchByUserName(String name) {
		UserDto userDto = null;
		StringBuilder sql = new StringBuilder();
		sql.append("select * from " + USER_TABLE_NAME + " where name = ?");
		try {
			Connection con = this.getConnection();
			PreparedStatement stmt;
			ResultSet rs;

			stmt = con.prepareStatement(sql.toString());
			stmt.setString(1, name);
			rs = stmt.executeQuery();
			while (rs.next()) {
				userDto = setUserDto(rs);
			}
			this.closeStatement(stmt);
			this.closeConnection(con);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return userDto;
	}

	/*
	 * search user by mail_address
	 * @param String mail_address
	 * @return userDto
	 */
	public UserDto searchByUserMailAddress(String mail_address) {
		UserDto userDto = null;
		StringBuilder sql = new StringBuilder();
		sql.append("select * from " + USER_TABLE_NAME + " where mail_address = ?");
		try {
			Connection con = this.getConnection();
			PreparedStatement stmt;
			ResultSet rs;

			stmt = con.prepareStatement(sql.toString());
			stmt.setString(1, mail_address);
			//			System.out.println(stmt.toString()); // テストプリント
			rs = stmt.executeQuery();
			while (rs.next()) {
				userDto = setUserDto(rs);
			}
			this.closeStatement(stmt);
			this.closeConnection(con);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return userDto;
	}

	/*
	 *  get user_id by user_name
	 *  @param String name
	 *  @return int user_id
	 */
	public int getUserId(String name) {
		int id = -1;
		StringBuilder sql = new StringBuilder();
		sql.append("select " + USER_ID + " from " + USER_TABLE_NAME + " where name = ?");
		try {
			Connection con = this.getConnection();
			PreparedStatement stmt;
			ResultSet rs;

			stmt = con.prepareStatement(sql.toString());
			stmt.setString(1, name);
			rs = stmt.executeQuery();
			while (rs.next())
				id = rs.getInt(USER_ID);
			this.closeStatement(stmt);
			this.closeConnection(con);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return id;
	}

	/*
	 * insert user
	 * @param UserDto userDto
	 */
	@SuppressWarnings("finally")
	public int insert(UserDto userDto) {
		int result = 0;
		StringBuilder sql = new StringBuilder();
		sql.append("insert into " + USER_TABLE_NAME + " values (DEFAULT, ?, ?, ?, ?, DEFAULT)");
		try {
			Connection con = this.getConnection();
			PreparedStatement stmt;

			stmt = con.prepareStatement(sql.toString());
			stmt.setString(1, userDto.getUserName());
			stmt.setString(2, userDto.getMailAddress());
			stmt.setString(3, userDto.getPassword());
			stmt.setInt(4, userDto.getGender());
			stmt.executeUpdate();
			this.closeStatement(stmt);
			this.closeConnection(con);
			new UserHobbyDao().insert(getUserId(userDto.getUserName()), userDto.getHobbyName());
		} catch (Exception e) {
			e.printStackTrace();
			result = -1;
		} finally {
			return result;
		}
	}

	/*
	 * user update
	 * @param int user_id, String pass, List<String> names
	 */
	public void update(int user_id, String pass, List<String> hobbyNames) {
		StringBuilder sql = new StringBuilder();
		sql.append("update " + USER_TABLE_NAME + " set " + USER_PASSWORD + " = ? where id  = ?");
		try {
			Connection con = this.getConnection();
			PreparedStatement stmt;

			if (pass != null) {
				stmt = con.prepareStatement(sql.toString());
				stmt.setString(1, pass);
				stmt.setInt(2, user_id);
				stmt.executeUpdate();
				this.closeStatement(stmt);
			}
			new UserHobbyDao().updateHobbyNameOfUser(user_id, hobbyNames);
			this.closeConnection(con);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// setter of UserDTO
	public UserDto setUserDto(ResultSet rs) throws SQLException {
		UserDto ud = new UserDto();
		ud.setUserId(rs.getInt(USER_ID));
		ud.setUserName(rs.getString(USER_NAME));
		ud.setMailAddress(rs.getString(USER_MAIL_ADDRESS));
		ud.setPassword(rs.getString(USER_PASSWORD));
		ud.setGender(rs.getInt(USER_GENDER));
		ud.setHobbyName(new UserHobbyDao().searchHobbyNamesByUserId(ud.getUserId()));
		return ud;
	}

	// get DB connection
	public Connection getConnection() throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.cj.jdbc.Driver");
		return DriverManager.getConnection(DB_URL, DB_USER, DB_PASS);
	}

	// close DB statement
	public void closeStatement(PreparedStatement stmt) throws SQLException {
		if (stmt != null)
			stmt.close();
	}

	// close DB connection;
	public void closeConnection(Connection con) throws SQLException {
		if (con != null)
			con.close();
	}
}
