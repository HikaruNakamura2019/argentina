package test;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.EventDao;
import dto.EventDto;

/** EventDaoTest
 * @author AZUMA Ginji
 */
@WebServlet(name="EventDaoTest", urlPatterns={"/EventDaoTest.java"})
public class EventDaoTest extends HttpServlet {
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		EventDao eventDao = new EventDao();
		// 全イベント情報の検索
//		System.out.println("selectAll テスト");
//		ArrayList<EventDto> events = eventDao.selectAll();
//		for (EventDto e : events) {
//			System.out.println("---------------------------------------------------------");
//			e.view();
//		}
//		System.out.println();

		// イベント ID によるイベント検索
//		System.out.println("\nselectById テスト");
//		EventDto event = eventDao.selectById(1);
//		System.out.println("---------------------------------------------------------");
//		event.view();
//		event = eventDao.selectById(2);
//		System.out.println("---------------------------------------------------------");
//		event.view();
//		if (eventDao.selectById(3).isEmpty())  System.out.println("ok");
//		if (eventDao.selectById(0).isEmpty()) System.out.println("ok");

		// 趣味名によるイベント検索
//		System.out.println("\nselectByHobby テスト");
//		events = eventDao.selectByHobby("酒");
//		System.out.println("酒で検索");
//		for (EventDto e : events) {
//			System.out.println("---------------------------------------------------------");
//			e.view();
//		}
//		System.out.println();
//		events = eventDao.selectByHobby("音楽");
//		System.out.println("音楽で検索");
//		for (EventDto e : events) {
//			System.out.println("---------------------------------------------------------");
//			e.view();
//		}
//		System.out.println();
//		events = eventDao.selectByHobby("あほ");
//		System.out.println("あほで検索");
//		if (events.isEmpty())  System.out.println("ok");
//		System.out.println();

		// ユーザ名から参加中のイベント一覧を取得する
//		System.out.println("\nselectByEntry テスト");
//		events = eventDao.selectByEntry("パク");
//		System.out.println("パクで検索");
//		for (EventDto e : events) {
//			System.out.println("---------------------------------------------------------");
//			e.view();
//		}
//		System.out.println();
//		events = eventDao.selectByEntry("ひかる");
//		System.out.println("ひかるで検索");
//		for (EventDto e : events) {
//			System.out.println("---------------------------------------------------------");
//			e.view();
//		}
//		System.out.println();
//		events = eventDao.selectByEntry("あずま");
//		System.out.println("あずまで検索");
//		if (events.isEmpty())  System.out.println("ok");
//		System.out.println();

		// 開催日時によるイベントの検索
//		System.out.println("\nselectByTime テスト");
//		try {
//			Timestamp oldTime = new Timestamp(new SimpleDateFormat("yyyy/MM/dd HH:mm").parse("2019/05/30 18:00").getTime());
//			Timestamp nextTime = new Timestamp(new SimpleDateFormat("yyyy/MM/dd HH:mm").parse("2019/06/03 12:00").getTime());
//			events = eventDao.selectByTime(oldTime, nextTime);
//			for (EventDto e : events) {
//				System.out.println("---------------------------------------------------------");
//				e.view();
//			}
//			System.out.println();
//
//			oldTime = new Timestamp(new SimpleDateFormat("yyyy/MM/dd HH:mm").parse("2019/05/30 18:01").getTime());
//			nextTime = new Timestamp(new SimpleDateFormat("yyyy/MM/dd HH:mm").parse("2019/06/03 12:00").getTime());
//			events = eventDao.selectByTime(oldTime, nextTime);
//			for (EventDto e : events) {
//				System.out.println("---------------------------------------------------------");
//				e.view();
//			}
//			System.out.println();
//
//			oldTime = new Timestamp(new SimpleDateFormat("yyyy/MM/dd HH:mm").parse("2019/05/30 18:00").getTime());
//			nextTime = new Timestamp(new SimpleDateFormat("yyyy/MM/dd HH:mm").parse("2019/06/03 11:59").getTime());
//			events = eventDao.selectByTime(oldTime, nextTime);
//			for (EventDto e : events) {
//				System.out.println("---------------------------------------------------------");
//				e.view();
//			}
//			System.out.println();
//
//			oldTime = new Timestamp(new SimpleDateFormat("yyyy/MM/dd HH:mm").parse("2019/06/03 12:00").getTime());
//			nextTime = new Timestamp(new SimpleDateFormat("yyyy/MM/dd HH:mm").parse("2019/06/03 12:00").getTime());
//			events = eventDao.selectByTime(oldTime, nextTime);
//			for (EventDto e : events) {
//				System.out.println("---------------------------------------------------------");
//				e.view();
//			}
//			System.out.println();
//		} catch (ParseException e1) {
//			e1.printStackTrace();
//		}

		// 日時と趣味で絞って検索
//		System.out.println("\nsearchEvent テスト");
//		try {
//			Timestamp oldTime = new Timestamp(new SimpleDateFormat("yyyy/MM/dd HH:mm").parse("2019/05/30 18:00").getTime());
//			Timestamp nextTime = new Timestamp(new SimpleDateFormat("yyyy/MM/dd HH:mm").parse("2019/06/03 12:00").getTime());
//			events = eventDao.searchEvent("酒", oldTime, nextTime);
//			for (EventDto e : events) {
//				System.out.println("---------------------------------------------------------");
//				e.view();
//			}
//			System.out.println();
//			events = eventDao.searchEvent("スポーツ", oldTime, nextTime);
//			for (EventDto e : events) {
//				System.out.println("---------------------------------------------------------");
//				e.view();
//			}
//			System.out.println();
//			events = eventDao.searchEvent("音楽", oldTime, nextTime);
//			for (EventDto e : events) {
//				System.out.println("---------------------------------------------------------");
//				e.view();
//			}
//			System.out.println();
//			events = eventDao.searchEvent("ハゲ", oldTime, nextTime);
//			for (EventDto e : events) {
//				System.out.println("---------------------------------------------------------");
//				e.view();
//			}
//			System.out.println();
//		} catch (ParseException e1) {
//			e1.printStackTrace();
//		}

		// イベントに対応する趣味を検索
//		System.out.println("///////////// hobbySelectById //////////////////////");
//		System.out.println("飲み会にタグ付けされてる趣味");
//		ArrayList<String> hobbys = eventDao.hobbySelectById(1);
//		for(String hobby : hobbys) {
//			System.out.print(hobby + ", ");
//		}
//		System.out.println();
//
//		System.out.println("タコパにタグ付けされてる趣味");
//		hobbys = eventDao.hobbySelectById(2);
//		for(String hobby : hobbys) {
//			System.out.print(hobby + ", ");
//		}
//		System.out.println();
//
//		System.out.println("存在しないイベントにタグ付けされてる趣味");
//		hobbys = eventDao.hobbySelectById(3);
//		for(String hobby : hobbys) {
//			System.out.print(hobby + ", ");
//		}
//		System.out.println();

		// イベントに参加しているユーザ一覧を取得
//		System.out.println("///////////// entrySelectById //////////////////////");
//		System.out.println("飲み会の参加者");
//		ArrayList<String> entrys = eventDao.entrySelectById(1);
//		for(String e : entrys) {
//			System.out.print(e + ", ");
//		}
//		System.out.println();
//
//		System.out.println("タコパの参加者");
//		entrys = eventDao.entrySelectById(2);
//		for(String e : entrys) {
//			System.out.print(e + ", ");
//		}
//		System.out.println();
//
//		System.out.println("存在しないイベントにタグ付けされてる趣味");
//		entrys = eventDao.entrySelectById(0);
//		for(String e : entrys) {
//			System.out.print(e + ", ");
//		}
//		System.out.println();

		// イベント情報を登録
//		System.out.println("/////////////////////////// insertEvent //////////////////////");
//		UserDao userDao = new UserDao();
//		EventDto eventDto = new EventDto();
//		eventDto.setTitle("イベント登録テスト");
//		eventDto.setContent("これはテストです。\nEventDaoの単体テストです。");
//		eventDto.setMaxEntry(10);
//		eventDto.setEventDate(new Timestamp(System.currentTimeMillis()));
//		eventDto.setCreateUser(userDao.searchByUserName("ひかる"));
//		hobbys = new ArrayList<String>(Arrays.asList("酒", "スポーツ", "音楽"));
//		eventDto.setHobbys(hobbys);
//		eventDao.insertEvent(eventDto);

		// イベント ID に一致するイベント情報を更新
		System.out.println("/////////////////////////// updateEvent //////////////////////");
		EventDto eventDto = new EventDto();
		eventDto.setEventId(3);
		eventDto.setTitle("イベント変更");
		eventDto.setContent("これは変更テストですよって。");
		eventDto.setMaxEntry(10);
		eventDto.setEventDate(new Timestamp(System.currentTimeMillis()));
		ArrayList<String> hobbys = new ArrayList<String>(Arrays.asList("スポーツ", "酒"));
		eventDto.setHobbys(hobbys);
		ArrayList<String> entryUsers = new ArrayList<>();
		entryUsers.add("ひかる");
		entryUsers.add("パク");
		eventDto.setEntryUsersName(entryUsers);
		eventDao.updateEvent(eventDto);
		eventDao.selectById(3).view();
	}
}