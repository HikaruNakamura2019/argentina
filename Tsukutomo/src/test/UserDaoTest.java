package test;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;
import dto.UserDto;

/** UserDaoTest
 * @author AZUMA Ginji
 */
@WebServlet(name = "UserDaoTest", urlPatterns = { "/UserDaoTest.java" })
public class UserDaoTest extends HttpServlet {
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		// searchByUserId
		printTestTitle("searchByUserId");
		searchByUserIdTest(1);
		System.out.println();
		searchByUserIdTest(2);
		System.out.println();
		searchByUserIdTest(3);
//		searchByUserIdTest(0); // ぬるぽ


		// searchByUserName
		printTestTitle("searchByUserName");
		searchByUserNameTest("パク");
		System.out.println();
		searchByUserNameTest("ひかる");
//		searchByUserNameTest("ひかる "); // ぬるぽ

		// searchByUserMailAddress
		printTestTitle("searchByUserMailAddress");
		searchByUserMailAddressTest("park@interspace.jp");
		System.out.println();
		searchByUserMailAddressTest("hikaru@interspace.jp");
//		searchByUserMailAddressTest(" park@interspace.jp");  // ぬるぽ

		// getUserId
		printTestTitle("getUserId");
		getUserIdTest("パク");
		getUserIdTest("ひかる");
		getUserIdTest(" パク"); // 結果なし

		// insert
//		printTestTitle("insert");
//		UserDto dto = new UserDto();
//		dto.setUserName("Hecate");
//		dto.setMailAddress("hecate@greece.mythology");
//		dto.setPassword("hecate");
//		dto.setGender(2);
//		dto.setHobbyName(Arrays.asList("音楽"));
//		insertTest(dto);

		// update
		printTestTitle("update");
		updateTest(2, "nakamura", Arrays.asList("酒", "音楽"));
		System.out.println();
		updateTest(2, "nakamura", Arrays.asList("酒", "スポーツ"));
//		updateTest(0, "hoge", Arrays.asList("酒", "スポーツ")); // ここは外部キー制約エラー
	}

	public static void printTestTitle(String title) {
		System.out.println("\n\n////////////////////////////// " + title + "  //////////////////////////////");
	}

	// searchByUserId のテスト
	public static void searchByUserIdTest(int userId) {
		UserDao dao = new UserDao();
		dao.searchByUserId(userId).view();
	}

	// searchByUserName のテスト
	public static void searchByUserNameTest(String name) {
		UserDao dao = new UserDao();
		dao.searchByUserName(name).view();
	}

	// searchByUserMailAddress のテスト
	public static void searchByUserMailAddressTest(String mail) {
		UserDao dao = new UserDao();
		dao.searchByUserMailAddress(mail).view();
	}

	// getUserId のテスト
	public static void getUserIdTest(String name) {
		UserDao dao = new UserDao();
		System.out.println("name: \"" + name + "\"    id: " + dao.getUserId(name));
	}

	// insert のテスト
	public static void insertTest(UserDto dto) {
		UserDao dao = new UserDao();
		dao.insert(dto);
		dao.searchByUserName(dto.getUserName()).view();
	}

	// update のテスト
	public static void updateTest(int userId, String pass, List<String> hobbyNames) {
		UserDao dao = new UserDao();
		dao.update(userId, pass, hobbyNames);
		searchByUserIdTest(userId);
	}
}
